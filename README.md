Status
----------

| Folder | Name |Gerbers | Proven |
| :--------| :------| :-----| :-----|
| [01lilsid](/papers/01lilsid/) | Lil Sidrassi | Yes | Yes
| [01lilsid](/papers/01lilsid/) | Lil Sidrassi (no LM386) | Yes | No
| [esoterica_chainlock](/papers/esoterica_chainlock/) | Esoterica Chainlock | Yes | Yes
| [esoterica_chainlock](/papers/esoterica_chainlock/) | Esoterica Chainlock (Tidied Version) | Yes | No
| [esoterica_spikering](/papers/esoterica_spikering/) | Esoterica Spikering | Yes | Yes
| [nobsrine](/papers/nobsrine/) | Nobsrine | Yes | Yes

_Note: Rollz-5 system is all done and proven but I've not got time to update documentation yet._

# Ciat Lonbarde PCBs

These are personal attempts to make PCBs from Ciat Lonbarde 'Paper Circuits. They are PCB manufacturing files derived from Peter Blasser's work. The repository contains folders and zips of 'Gerber' files for PCB manufacture. Not all of the work is finished yet. You can just upload the finished zip files to manufacturer's websites and order PCBs. If you want to do everything from scratch yourself, or just understand what I did, read on.

For more information on Peter B/Ciat Lonbarde have a browse here - https://www.ciat-lonbarde.net/

I am riding off of the work of many others. None of the circuit designs are mine, they are Peter B/Ciat Lonbarde's. Shared freely online, there is no detail on rights or limitations of usage. Whilst they are obviously intended to promote DIY tinkering, I don't know about commercial use (e.g. selling circuit boards) so don't endorse it.

Paper circuits come from: http://ciat-lonbarde.net/paper/ and http://www.ciat-lonbarde.net/TIMARACURRICULUM/TIMARATERIALS/cirques/index.html

To create PCB layouts, I started here: https://llllllll.co/t/mobenthey-ciat-lonbarde-synthmall-thread/3322/530?u=mlogger. kudos to mlogger/mudlogger, Jason Taylor, and a prod in this direction from Phil Julian.

Whilst I use a different method, I couldn't have gotten here without the work mentioned above.

Finally, I use Osmond PCB which is Mac only software. I use this because this is how the circuits were designed. Also, my initial interest here was in reverse engineering Blasser's work to learn how to use, and design my own parts libraries, in Osmond. The software is free and so, I thank the developers and recommend donating if you use it - https://www.osmondpcb.com/

I will be publishing a tutorial elsewhere and post the link when ready. In the meantime, below is a basic 'recipe' for creating Gerber files, for PCB manufacturing, from Blasser's files. I use JLCPCB and uploading a zip of Gerber files has worked well so far. I have successfully ordered, built, and tested Lil Sidrassi. Rollz-5 is ordered but not yet shipped. Everything documented here is, so far, largely unproven. YMMV.

## Osmond Method

First, rename the files with the .osm extension. That's the extension used by Osmond. Go ahead and open the file.

I find it easier to set visibility of all layers to 'No', that way you only see the active layer and you can run through them individually to check things as you go (View > Layers Visible...).

Some methods call for making PCBs that are double layer but I don't see any benefit. We do need to move the solder trace layer to the bottom though. Select layer 1 and select Design > Dup Layer Before.

We also need to fix the solder masks. Select layer M1. Cmd+A (or Edit > Select All) followed by Cmd+C (or Edit > Copy). Next select layer M2. Don't click anywhere or it'll set the origin point for pasting and things may be out of alignment. Cmd+V (or Edit > Paste). It may have pasted some text too (on the Rollz it pastes the name of the board), if so, click on the text and delete (backspace).

Finally, we need to draw the board edge. Select layer A1. Click on the pen tool. Draw a box around the edge of the design (right up to the edges).

You can make all the layers visible now for any final checks.

Next we want all the gerber files labelled correctly when we export. Go to Edit > Gerber File Names... and follow the following naming convention (where foo is the project name i.e. 01lilsid)

* Drill File: foo.drills_pth.xln
* Auxiliary Layer 1: foo.boardoutline.ger
* Auxiliary Layer 2: AUX2.GBR (Leave as is)
* Silkscreen Layer 1: foo.topsilkscreen.ger
* Silkscreen Layer 2: foo.bottomsilkscreen.ger
* Solder Mask Layer 1: foo.topsoldermask.ger
* Solder Mask Layer 2: foo.bottomsoldermask.ger
* Layer 1: foo.toplayer.ger
* Layer 2: foo.bottomlayer.ger
* Layer 3: LAYER3.GBR (Leave as is)
* Layer 4: LAYER4.GBR (Leave as is)
* Layer X: LAYER%d.GBR (Leave as is)

There's a handy gSheet for calculating these here - https://docs.google.com/spreadsheets/d/1oP2Sg7Z93opfU3iwMOnj0NdZfqy-qCHH4izzCqyGZlc/edit?usp=sharing

Finally, go to File > Export > Gerber Files... and leave the standard selections. You should now have a folder filled with Gerber files suitable for PCB manufacturers.

I find it easiest to zip them all up and use that.

## Old Method

I've kept this for reference. This is how I started out, following the instructions from the llllllll forum. It uses a combination of exporting from Osmond and then making alterations in KiCad. I found this unnecessary. You might not.

~~In brief I am renaming all of the files with the .osm extension and loading in Osmond (mac only). I'm then exporting to gerber.~~

~~Next I duplicate (in finder) LAYER1 and MASK1. I rename these to LAYER2.GBL and MASK2.GBS (note the file extensions).~~

~~I then change the following files to these extensions: DRILL.TXT, AUX1.GM1, AUX2.GBR, SILK1.GTO, SILK2.GTO, MASK1.GTS, LAYER1.GTL~~

~~There are other files in the folder but I leave these as they are as they're not really used.~~

~~I then use KiCad to open up these in gerbview. You will see the finished board and can check for problems etc.~~

~~Next, and this is in addition to mlogger's steps, I had problems with the board edges not being defined so I export to pcbnew from gerbview.~~

~~I open the exported file, select the Edge.Cuts layer and draw a box around the circuit board. I save this and then go to plot. In the plot settings I deselect all layers except Edge.Cuts and ensure Exclude PCB edge layer from other layers is selected. I select the output path to be my gerber folder from earlier and then hit Plot.~~

~~In Finder I rename the edge cuts gerber file with the GKO extension.~~

~~There may be an alternative method from dianus here https://llllllll.co/t/mobenthey-ciat-lonbarde-synthmall-thread/3322/540 but my first attempt didn't work and I'll have to re-investigate when I have more time (note: I'm no longer going to look into this as I've got a different method I prefer).~~

~~I then make a zip of the gerber files folder and this is the finished PCB layout that can be uploaded to manufacturing sites.~~

(test for commit)
