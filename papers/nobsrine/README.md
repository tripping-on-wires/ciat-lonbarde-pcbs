# Nobsrine

![Nobsrine](nobsrine.png)

There isn't an Osmond file for this circuit, though there is a printable paper version.

When building one on paper I found it a little bit over-complex for a paper build and thought a PCB might be preferable. I have therefore recreated the file in Osmond (I used the pdf import function and overlaid components/traces to try and match).

I have added in two 10M resistors, as indicated in this thread [https://llllllll.co/t/mobenthey-ciat-lonbarde-synthmall-thread/3322/1608?u=trippingonwires](https://llllllll.co/t/mobenthey-ciat-lonbarde-synthmall-thread/3322/1608?u=trippingonwires) (thanks to Coitmusic and original credit to barrford).

The original paper has several 'Air Jordan' connections due to paper's conductivity. I have replicated these on the top layer instead.

I have double checked all of the connections and this has kindly been looked over by barrford.

~~I have ordered some PCBs but until thy arrive and I can prove a build this is untested and so no promises it will work.~~ I have now built this and can confirm it works.

[Video snippet of nobsrine in action](https://www.instagram.com/p/CM7BInQhB5s/?utm_source=ig_web_copy_link)

I've explored patching, corrected the wrong diode part number, and confirmed a few more details so updated the BOM and notes.
