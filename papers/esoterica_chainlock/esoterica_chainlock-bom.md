Format for this file is:
  designation in Osmond PCB, part type, value, count

__Note: all counts (or most counts) are doubled due to duplicated parts in the OSM file. I'll update as things develop but there's also an esoterica_chainlock_tidy version addressing this. Check out the main README for more details.__

_Also: I haven't built this yet so haven't updated any info regarding mods or X/Y values etc._

Capacitors
----------

| | Type | Value | Count |
| :--------| :------| :-----| :-----|
| cpoly103 | Mylar polyester | 10nF | 6
| cpoly472 | Mylar polyester | 4.7nF | 4
| cpoly104 | Mylar polyester | 100nF | 8
| cel1 | Electrolytic capacitor | 10µF | 6
| | | | __24__ |

Resistors
---------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| r3 | Carbon film 1/4W | 4.7K | 16
| r3 | Carbon film 1/4W | 10K | 16
| r3 | Carbon film 1/4W | 22K | 36
| r3 | Carbon film 1/4W | 47K | 24
| r3 | Carbon film 1/4W | 100K | 4
| r3 | Carbon film 1/4W | 220K | 12
| r3 | Carbon film 1/4W | 470K | 2
| r3 | Carbon film 1/4W | 1M | 2
| r3 | Carbon film 1/4W | 2.2M | 8
| r3 | Carbon film 1/4W | 10M | 2
| r3 | Carbon film 1/4W | X | 2
| r3 | Carbon film 1/4W | Y | 2
| r3 | Jumper wire | 0 | 2
| r3 | Jumper wire |  | 2
| | | | __130__

_Notes: There's a 0 and a blank resistor which are both jumper wires._

Diodes
------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| do3 | Signal diode | 1n914 | 10
||||__10__


Transistors
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| qpnp | PNP transistor | BC557B | 20
| qnpn | NPN transistor | BC547B | 12
||||__32__

_Note: PNP can also be BC556 or BC558, and NPN can also be BC546 or BC548._

Semiconductor ICs
-----------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| dip14 |  JFET input quad opamp | TL064 | 4
| dip14 |  Dual D-type flip-flop | CD4013 | 2
| dip16 |  Dual operational transconductance amps | LM13700 | 2
||||__8__

Potentiometers
--------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wnob | Linear potentiometer | 10K | 4
||||__4__

Power input
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wobelisk | battery clip | 9v | 2
||||__2__

Outputs
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wpod| Mono audio jack | 3.5mm or 1/4" | 2
||||__2__

Connections (no parts, possibly banana connectors etc.)
--------------------------------------------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wsqin || Touch points or control inputs| 2
| wtrin  || Touch points or control inputs| 2
| wsquin|| Touch points or control inputs| 2
| wsqu2in|| Touch points or control inputs | 2
| wttrchaos||Touch points or control inputs| 2
| wtttrrugg||Touch points or control inputs|2
||||__10__

_Note: wsqin looks a bit like a button but doesn't function this way. Generally you only want to patch one of the connections at a time and it seems to change the overall pitch/feel of the circuit, whereas both tend to cancel each other out a bit. Everything else works best as a regular patch point. There's nothing much going on from touch though so you'll want to patch to something external like a Rollz. I've marked as touch points above simply as you may want to use touch to patch, rather than bananas or 3.5mm jacks._
