Format for this file is:
  designation in Osmond PCB, part type, value, count

_Note: I haven't built this yet so haven't updated any info regarding mods or X/Y values etc._

Capacitors
----------

| | Type | Value | Count |
| :--------| :------| :-----| :-----|
| cpoly103 | Mylar polyester | 10nF | 3
| cpoly472 | Mylar polyester | 4.7nF | 2
| cpoly104 | Mylar polyester | 100nF | 4
| cel1 | Electrolytic capacitor | 10µF | 3
| | | | __12__ |

Resistors
---------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| r3 | Carbon film 1/4W | 4.7K | 8
| r3 | Carbon film 1/4W | 10K | 8
| r3 | Carbon film 1/4W | 22K | 18
| r3 | Carbon film 1/4W | 47K | 12
| r3 | Carbon film 1/4W | 100K | 2
| r3 | Carbon film 1/4W | 220K | 6
| r3 | Carbon film 1/4W | 470K | 1
| r3 | Carbon film 1/4W | 1M | 1
| r3 | Carbon film 1/4W | 2.2M | 4
| r3 | Carbon film 1/4W | 10M | 1
| r3 | Carbon film 1/4W | X | 1
| r3 | Carbon film 1/4W | Y | 1
| r3 | Jumper wire | 0 | 1
| r3 | Jumper wire |  | 1
| | | | __65__

_Notes: There's a 0 and a blank resistor which are both jumper wires._

Diodes
------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| do3 | Signal diode | 1n914 | 5
||||__5__


Transistors
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| qpnp | PNP transistor | BC557B | 10
| qnpn | NPN transistor | BC547B | 6
||||__16__

_Note: PNP can also be BC556 or BC558, and NPN can also be BC546 or BC548._

Semiconductor ICs
-----------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| dip14 |  JFET input quad opamp | TL064 | 2
| dip14 |  Dual D-type flip-flop | CD4013 | 1
| dip16 |  Dual operational transconductance amps | LM13700 | 1
||||__4__

Potentiometers
--------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wnob | Linear potentiometer | 10K | 2
||||__2__

_Note: This is completely unconfirmed. I'm considering 10K based upon other builds._

Power input
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wobelisk | battery clip | 9v | 1
||||__1__

Outputs
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wpod| Mono audio jack | 3.5mm or 1/4" | 1
||||__1__

Connections (no parts, possibly banana connectors etc.)
--------------------------------------------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wsqin || Touch points or control inputs| 1
| wtrin ||Touch points or control inputs| 1
| wsquin||Touch points or control inputs| 1
| wsqu2in||Touch points or control inputs| 1
| wttrchaos||Touch points or control inputs| 1
| wtttrrugg||Touch points or control inputs| 1
||||__5__

_Note: wsqin looks a bit like a button but doesn't function this way. Generally you only want to patch one of the connections at a time and it seems to change the overall pitch/feel of the circuit, whereas both tend to cancel each other out a bit. Everything else works best as a regular patch point. There's nothing much going on from touch though so you'll want to patch to something external like a Rollz. I've marked as touch points above simply as you may want to use touch to patch, rather than bananas or 3.5mm jacks._
