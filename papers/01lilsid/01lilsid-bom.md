Format for this file is:
  designation in Osmond PCB, part type, value, count

Capacitors
----------

| | Type | Value | Count |
| :--------| :------| :-----| :-----|
| cpoly104 | Mylar polyester | 100nF | 7
| chairy | See notes | See notes | 5
| cel1 | Electrolytic capacitor | 10µF | 1
| cel2 | Electrolytic capacitor | 470µF | 1
| | | | __14__ |
_Notes: chairy can be any value/type of capacitor. They can be a mix of values or all the same. 100pF is in the radio ultrasound range, 1nF ultrasound, 10nF treble, 100nF bass, 1µF subbass, 10µF subbubbass, and 100µF mountain sub._

Resistors
---------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| r3 | Carbon film 1/4W | 2.2k | 11
| r3 | Carbon film 1/4W | 4.7k | 2
| r3 | Carbon film 1/4W | 22k | 1
| r3 | Carbon film 1/4W | 47k | 5
| r3 | Carbon film 1/4W | 220k | 5
| r3 | Carbon film 1/4W | 470k | 5
| r3 | Carbon film 1/4W | 1m | 6
| r3 | Carbon film 1/4W | 2.2m | 5
| r3 | Jumper wire | 0 | 1
| | | | __41__


Diodes
------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| do3 | Signal diode | 1n4148 | 1
||||__1__

Transistors
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| qpnp | PNP transistor | BC557B | 10
| qnpn | NPN transistor | BC547B | 5
| qfet | FET Transistor | 2N3819 |  1
||||__16__

_Note: PNP can also be BC556 or BC558, and NPN can also be BC546 or BC548._

Semiconductor ICs
-----------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| dip8 |  Audio power amplifier | LM386 | 1
||||__1__

Power input
-----------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| U11 | battery clip | 9v | 1
||||__1__

Outputs
-------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| U9| Speaker connector | Wire to a speaker | 1
||||__1__
_Notes: The speaker connector can be wired to a jack socket instead. Square hole to sleeve, round hole to tip. The sound output, via the LM386 is notriously bad though so you might want to check out the no LM386 version also in thsi repository._

Connections (no parts, possibly banana connectors etc.)
--------------------------------------------------

| | Type | Value | Count |
| :------| :-----| :-----| :----|
| wsquin  ||| 5
| wsqout||| 5
| wtrin  ||| 1
||||__11__
_Notes: Generally wired to touch points (e.g. brass pegs), loose wires, banana connectors or 3.5mm jacks (for eurorack style cables). Making connections (and touch capacitance) allows for sonic effects and gestural play._
