# Lil Sidrassi

## 01lilsid

![Lil Sidrassi](01lilsid.png)

This is the first paper circuit I turned into a PCB. I have had a number of these boards made and they work. I have since updated several pins to be 80mil with 40mil holes. This is to accommodate 22AWG hookup wire. I have applied this only to the 'strong force', 'weak force', 'speaker sensor', speaker hookup 'castle', and battery snap connection. Basically all the bits where you'll probably need hookup wire rather than actual components. Finally, I have also done this for all 5 'hairy' capacitors to make life easier when wiring up switches (to switch between different capacitors).

The gerber files (and zip) have also been updated.

As I haven't changed any connections etc. I would still consider this proven to work.

## 01lilsid (no lm386)

![Lil Sidrassi](01lilsid%20(no%20lm386).png)

I have also created a 'No LM386' version. I wired the original Lil Sidrassi output to an audio jack but the sound quality was pretty bad. In particular, I found connecting it to a Koma Field Kit produced large grounding issues. I played with it at a show in Hamburg once and it was incredibly flaky on the house PA. Since then I figured out how to bypass the LM386 and take the output straight from the FET amplifier stage. I later noticed that Richard Brewster had done the same so I'm confident this is a good approach.

I've wired numerous of the original boards this way and it works like a charm.

This version removes the unnecessary components and reroutes the signals as appropriate. This particular PCB build is untested and I'll update this page once proven.
