# Notes for Rollz-5 PCBs.

The 5-Roll, Gongs, and Ultrasound Filters mentions an error, when exporting Gerber files, about not all of the paths being connected. I'll see how this turns out in fabrication and comment here further.

There's a known issue with the 6-Roll where the 1µF electolytic capacitor is the wrong way around. I have corrected this by switching the connections and rotating the part.

There was a file called osmond which appears to be a blank file. There was also a folder called RESOURCE.FRK with a file called osmond which again appears to be a blank file. These are artefacts from working between Mac and PC filesystems. I am working on a Mac and everything works without them so I have removed these from the repository. If you want them, you can download via http://www.ciat-lonbarde.net/TIMARACURRICULUM/TIMARATERIALS/cirques/index.html.
